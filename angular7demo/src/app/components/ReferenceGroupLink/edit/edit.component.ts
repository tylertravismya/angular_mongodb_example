import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReferenceGroupLinkService } from '../../../services/ReferenceGroupLink.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../ReferenceGroupLink/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditReferenceGroupLinkComponent extends SubBaseComponent implements OnInit {

  referenceGroupLink: any;
  referenceGroupLinkForm: FormGroup;
  title = 'Edit ReferenceGroupLink';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferenceGroupLinkService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.referenceGroupLinkForm = this.fb.group({
      dateLinkCreated: ['', Validators.required],
      name: ['', Validators.required],
      ReferrerGroup: ['', ],
      LinkProvider: ['', ]
   });
  }
  updateReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider) {
    this.route.params.subscribe(params => {
    	this.service.updateReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider, params['id'])
      		.then(success => this.router.navigate(['/indexReferenceGroupLink']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.referenceGroupLink = this.service.editReferenceGroupLink(params['id']).subscribe(res => {
        this.referenceGroupLink = res;
      });
    });
    
    super.ngOnInit();
  }
}
