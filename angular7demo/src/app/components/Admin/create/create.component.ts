import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/Admin.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Admin/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateAdminComponent extends SubBaseComponent implements OnInit {

  title = 'Add Admin';
  adminForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private adminservice: AdminService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.adminForm = this.fb.group({
      loginId: ['', Validators.required],
      password: ['', Validators.required],
      Users: ['', ],
      ReferenceEngines: ['', ]
   });
  }
  addAdmin(loginId, password, Users, ReferenceEngines) {
      this.adminservice.addAdmin(loginId, password, Users, ReferenceEngines)
      	.then(success => this.router.navigate(['/indexAdmin']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
