import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../../services/Admin.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Admin/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditAdminComponent extends SubBaseComponent implements OnInit {

  admin: any;
  adminForm: FormGroup;
  title = 'Edit Admin';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AdminService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.adminForm = this.fb.group({
      loginId: ['', Validators.required],
      password: ['', Validators.required],
      Users: ['', ],
      ReferenceEngines: ['', ]
   });
  }
  updateAdmin(loginId, password, Users, ReferenceEngines) {
    this.route.params.subscribe(params => {
    	this.service.updateAdmin(loginId, password, Users, ReferenceEngines, params['id'])
      		.then(success => this.router.navigate(['/indexAdmin']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.admin = this.service.editAdmin(params['id']).subscribe(res => {
        this.admin = res;
      });
    });
    
    super.ngOnInit();
  }
}
