import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ResponseOptionService } from '../../../services/ResponseOption.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../ResponseOption/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateResponseOptionComponent extends SubBaseComponent implements OnInit {

  title = 'Add ResponseOption';
  responseOptionForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private responseOptionservice: ResponseOptionService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.responseOptionForm = this.fb.group({
      responseText: ['', Validators.required],
      gotoQuestionId: ['', Validators.required]
   });
  }
  addResponseOption(responseText, gotoQuestionId) {
      this.responseOptionservice.addResponseOption(responseText, gotoQuestionId)
      	.then(success => this.router.navigate(['/indexResponseOption']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
