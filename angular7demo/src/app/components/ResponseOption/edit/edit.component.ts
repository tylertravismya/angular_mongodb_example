import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseOptionService } from '../../../services/ResponseOption.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../ResponseOption/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditResponseOptionComponent extends SubBaseComponent implements OnInit {

  responseOption: any;
  responseOptionForm: FormGroup;
  title = 'Edit ResponseOption';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ResponseOptionService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.responseOptionForm = this.fb.group({
      responseText: ['', Validators.required],
      gotoQuestionId: ['', Validators.required]
   });
  }
  updateResponseOption(responseText, gotoQuestionId) {
    this.route.params.subscribe(params => {
    	this.service.updateResponseOption(responseText, gotoQuestionId, params['id'])
      		.then(success => this.router.navigate(['/indexResponseOption']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.responseOption = this.service.editResponseOption(params['id']).subscribe(res => {
        this.responseOption = res;
      });
    });
    
    super.ngOnInit();
  }
}
