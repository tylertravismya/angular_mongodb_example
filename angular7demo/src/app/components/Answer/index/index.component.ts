import { AnswerService } from '../../../services/Answer.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Answer } from '../../../models/Answer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexAnswerComponent implements OnInit {

  answers: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AnswerService) {}

  ngOnInit() {
    this.getAnswers();
  }

  getAnswers() {
    this.service.getAnswers().subscribe(res => {
      this.answers = res;
    });
  }

  deleteAnswer(id) {
    this.service.deleteAnswer(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexAnswer']));
			});  }
}
