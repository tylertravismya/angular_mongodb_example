import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ReferrerGroupService } from '../../../services/ReferrerGroup.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../ReferrerGroup/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateReferrerGroupComponent extends SubBaseComponent implements OnInit {

  title = 'Add ReferrerGroup';
  referrerGroupForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private referrerGroupservice: ReferrerGroupService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.referrerGroupForm = this.fb.group({
      name: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      References: ['', ]
   });
  }
  addReferrerGroup(name, dateTimeLastViewedExternally, References) {
      this.referrerGroupservice.addReferrerGroup(name, dateTimeLastViewedExternally, References)
      	.then(success => this.router.navigate(['/indexReferrerGroup']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
