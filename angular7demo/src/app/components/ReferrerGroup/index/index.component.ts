import { ReferrerGroupService } from '../../../services/ReferrerGroup.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReferrerGroup } from '../../../models/ReferrerGroup';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexReferrerGroupComponent implements OnInit {

  referrerGroups: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferrerGroupService) {}

  ngOnInit() {
    this.getReferrerGroups();
  }

  getReferrerGroups() {
    this.service.getReferrerGroups().subscribe(res => {
      this.referrerGroups = res;
    });
  }

  deleteReferrerGroup(id) {
    this.service.deleteReferrerGroup(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexReferrerGroup']));
			});  }
}
