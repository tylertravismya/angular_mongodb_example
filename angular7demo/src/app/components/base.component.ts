import { HttpClient } from '@angular/common/http';
import * as enumTypes from '../models/EnumTypes';

import {UserService} from '../services/User.service';
import {ReferrerService} from '../services/Referrer.service';
import {ReferenceGiverService} from '../services/ReferenceGiver.service';
import {ReferrerGroupService} from '../services/ReferrerGroup.service';
import {ReferenceEngineService} from '../services/ReferenceEngine.service';
import {QuestionService} from '../services/Question.service';
import {ResponseOptionService} from '../services/ResponseOption.service';
import {QuestionGroupService} from '../services/QuestionGroup.service';
import {AdminService} from '../services/Admin.service';
import {ActivityService} from '../services/Activity.service';
import {CommentService} from '../services/Comment.service';
import {AnswerService} from '../services/Answer.service';
import {ReferenceGroupLinkService} from '../services/ReferenceGroupLink.service';

/** 
	Base class of all Components.  
	For convenience, contains all enums and entity lists 
**/
export class BaseComponent {

    constructor (private http: HttpClient) {}

// enum instances
    ProfessionTypes = Object.keys(enumTypes.ProfessionType);
    ReferenceStatuss = Object.keys(enumTypes.ReferenceStatus);
    Purposes = Object.keys(enumTypes.Purpose);
    ActivityTypes = Object.keys(enumTypes.ActivityType);

// all collection instances
    users : any;
    referrers : any;
    referenceGivers : any;
    referrerGroups : any;
    referenceEngines : any;
    questions : any;
    responseOptions : any;
    questionGroups : any;
    admins : any;
    activitys : any;
    comments : any;
    answers : any;
    referenceGroupLinks : any;
  
// initialization  
    ngOnInit() {
    }

    initUserList() {
        if ( this.users == null ) {
            new UserService(this.http).getUsers().subscribe(res => {
                this.users = res;
            });
        }
    }
    
    initReferrerList() {
        if ( this.referrers == null ) {
            new ReferrerService(this.http).getReferrers().subscribe(res => {
                this.referrers = res;
            });
        }
    }
    
    initReferenceGiverList() {
        if ( this.referenceGivers == null ) {
            new ReferenceGiverService(this.http).getReferenceGivers().subscribe(res => {
                this.referenceGivers = res;
            });
        }
    }
    
    initReferrerGroupList() {
        if ( this.referrerGroups == null ) {
            new ReferrerGroupService(this.http).getReferrerGroups().subscribe(res => {
                this.referrerGroups = res;
            });
        }
    }
    
    initReferenceEngineList() {
        if ( this.referenceEngines == null ) {
            new ReferenceEngineService(this.http).getReferenceEngines().subscribe(res => {
                this.referenceEngines = res;
            });
        }
    }
    
    initQuestionList() {
        if ( this.questions == null ) {
            new QuestionService(this.http).getQuestions().subscribe(res => {
                this.questions = res;
            });
        }
    }
    
    initResponseOptionList() {
        if ( this.responseOptions == null ) {
            new ResponseOptionService(this.http).getResponseOptions().subscribe(res => {
                this.responseOptions = res;
            });
        }
    }
    
    initQuestionGroupList() {
        if ( this.questionGroups == null ) {
            new QuestionGroupService(this.http).getQuestionGroups().subscribe(res => {
                this.questionGroups = res;
            });
        }
    }
    
    initAdminList() {
        if ( this.admins == null ) {
            new AdminService(this.http).getAdmins().subscribe(res => {
                this.admins = res;
            });
        }
    }
    
    initActivityList() {
        if ( this.activitys == null ) {
            new ActivityService(this.http).getActivitys().subscribe(res => {
                this.activitys = res;
            });
        }
    }
    
    initCommentList() {
        if ( this.comments == null ) {
            new CommentService(this.http).getComments().subscribe(res => {
                this.comments = res;
            });
        }
    }
    
    initAnswerList() {
        if ( this.answers == null ) {
            new AnswerService(this.http).getAnswers().subscribe(res => {
                this.answers = res;
            });
        }
    }
    
    initReferenceGroupLinkList() {
        if ( this.referenceGroupLinks == null ) {
            new ReferenceGroupLinkService(this.http).getReferenceGroupLinks().subscribe(res => {
                this.referenceGroupLinks = res;
            });
        }
    }
    
    
// comparison function for select controls  
    compareFn(user1: any, user2: any) {
        return user1 == user2
    }    
}
