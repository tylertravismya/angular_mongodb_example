import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { QuestionGroupService } from '../../../services/QuestionGroup.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../QuestionGroup/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateQuestionGroupComponent extends SubBaseComponent implements OnInit {

  title = 'Add QuestionGroup';
  questionGroupForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private questionGroupservice: QuestionGroupService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.questionGroupForm = this.fb.group({
      aggregateScore: ['', Validators.required],
      weight: ['', Validators.required],
      name: ['', Validators.required],
      Questions: ['', ],
      QuestionGroups: ['', ]
   });
  }
  addQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups) {
      this.questionGroupservice.addQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups)
      	.then(success => this.router.navigate(['/indexQuestionGroup']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
