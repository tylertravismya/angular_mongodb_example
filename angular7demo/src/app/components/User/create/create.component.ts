import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/User.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../User/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateUserComponent extends SubBaseComponent implements OnInit {

  title = 'Add User';
  userForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private userservice: UserService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.userForm = this.fb.group({
      password: ['', Validators.required],
      resumeLinkUrl: ['', Validators.required],
      linkedInUrl: ['', Validators.required],
      ReferenceProviders: ['', ],
      RefererGroups: ['', ],
      ReferenceReceivers: ['', ],
      ReferenceGroupLinks: ['', ]
   });
  }
  addUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks) {
      this.userservice.addUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks)
      	.then(success => this.router.navigate(['/indexUser']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
