import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentService } from '../../../services/Comment.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Comment/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditCommentComponent extends SubBaseComponent implements OnInit {

  comment: any;
  commentForm: FormGroup;
  title = 'Edit Comment';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: CommentService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.commentForm = this.fb.group({
      commentText: ['', Validators.required],
      Source: ['', ]
   });
  }
  updateComment(commentText, Source) {
    this.route.params.subscribe(params => {
    	this.service.updateComment(commentText, Source, params['id'])
      		.then(success => this.router.navigate(['/indexComment']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.comment = this.service.editComment(params['id']).subscribe(res => {
        this.comment = res;
      });
    });
    
    super.ngOnInit();
  }
}
