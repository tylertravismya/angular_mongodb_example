import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReferenceGiverService } from '../../../services/ReferenceGiver.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../ReferenceGiver/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditReferenceGiverComponent extends SubBaseComponent implements OnInit {

  referenceGiver: any;
  referenceGiverForm: FormGroup;
  title = 'Edit ReferenceGiver';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferenceGiverService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.referenceGiverForm = this.fb.group({
      dateLastUpdated: ['', Validators.required],
      active: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      makeViewableToUser: ['', Validators.required],
      dateLastSent: ['', Validators.required],
      numDaysToExpire: ['', Validators.required],
      QuestionGroup: ['', ],
      Status: ['', ],
      Type: ['', ],
      User: ['', ],
      Referrer: ['', ],
      Answers: ['', ],
      LastQuestionAnswered: ['', ]
   });
  }
  updateReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) {
    this.route.params.subscribe(params => {
    	this.service.updateReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered, params['id'])
      		.then(success => this.router.navigate(['/indexReferenceGiver']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.referenceGiver = this.service.editReferenceGiver(params['id']).subscribe(res => {
        this.referenceGiver = res;
      });
    });
    
    super.ngOnInit();
  }
}
