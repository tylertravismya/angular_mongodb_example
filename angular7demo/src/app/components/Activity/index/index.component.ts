import { ActivityService } from '../../../services/Activity.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Activity } from '../../../models/Activity';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexActivityComponent implements OnInit {

  activitys: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ActivityService) {}

  ngOnInit() {
    this.getActivitys();
  }

  getActivitys() {
    this.service.getActivitys().subscribe(res => {
      this.activitys = res;
    });
  }

  deleteActivity(id) {
    this.service.deleteActivity(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexActivity']));
			});  }
}
