var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for ReferenceGroupLink
var ReferenceGroupLink = new Schema({
  dateLinkCreated: {
	type : String
  },
  name: {
	type : String
  },
  ReferrerGroup: {
	type : Schema.Types.ObjectId
  },
  LinkProvider: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'referenceGroupLinks'
});

module.exports = mongoose.model('ReferenceGroupLink', ReferenceGroupLink);