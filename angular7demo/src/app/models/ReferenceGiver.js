var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for ReferenceGiver
var ReferenceGiver = new Schema({
  dateLastUpdated: {
	type : String
  },
  active: {
	type : Boolean
  },
  dateTimeLastViewedExternally: {
	type : String
  },
  makeViewableToUser: {
	type : Boolean
  },
  dateLastSent: {
	type : String
  },
  numDaysToExpire: {
	type : Number
  },
  QuestionGroup: {
	type : Schema.Types.ObjectId
  },
  Status: {
 	type : String
  },
  Type: {
 	type : String
  },
  User: {
	type : Schema.Types.ObjectId
  },
  Referrer: {
	type : Schema.Types.ObjectId
  },
  Answers: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Answer' }]
  },
  LastQuestionAnswered: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'referenceGivers'
});

module.exports = mongoose.model('ReferenceGiver', ReferenceGiver);