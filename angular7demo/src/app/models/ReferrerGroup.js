var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for ReferrerGroup
var ReferrerGroup = new Schema({
  name: {
	type : String
  },
  dateTimeLastViewedExternally: {
	type : String
  },
  References: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'ReferenceGiver' }]
  },
},{
    collection: 'referrerGroups'
});

module.exports = mongoose.model('ReferrerGroup', ReferrerGroup);