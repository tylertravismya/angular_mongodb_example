var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Admin
var Admin = new Schema({
  loginId: {
	type : String
  },
  password: {
	type : String
  },
  Users: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'User' }]
  },
  ReferenceEngines: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'ReferenceEngine' }]
  },
},{
    collection: 'admins'
});

module.exports = mongoose.model('Admin', Admin);