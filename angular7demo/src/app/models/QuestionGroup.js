var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for QuestionGroup
var QuestionGroup = new Schema({
  aggregateScore: {
	type : String
  },
  weight: {
	type : String
  },
  name: {
	type : String
  },
  Questions: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Question' }]
  },
  QuestionGroups: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'QuestionGroup' }]
  },
},{
    collection: 'questionGroups'
});

module.exports = mongoose.model('QuestionGroup', QuestionGroup);