// referrerRoutes.js

var express = require('express');
var app = express();
var referrerRoutes = express.Router();

// Require Item model in our routes module
var Referrer = require('../models/Referrer');

// Defined store route
referrerRoutes.route('/add').post(function (req, res) {
	var referrer = new Referrer(req.body);
	referrer.save()
    .then(item => {
    	res.status(200).json({'referrer': 'Referrer added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
referrerRoutes.route('/').get(function (req, res) {
	Referrer.find(function (err, referrers){
		if(err){
			console.log(err);
		}
		else {
			res.json(referrers);
		}
	});
});

// Defined edit route
referrerRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Referrer.findById(id, function (err, referrer){
		res.json(referrer);
	});
});

//  Defined update route
referrerRoutes.route('/update/:id').post(function (req, res) {
	Referrer.findById(req.params.id, function(err, referrer) {
		if (!referrer)
			return next(new Error('Could not load a Referrer Document using id ' + req.params.id));
		else {
            referrer.firstName = req.body.firstName;
            referrer.lastName = req.body.lastName;
            referrer.emailAddress = req.body.emailAddress;
            referrer.active = req.body.active;
            referrer.Comments = req.body.Comments;

			referrer.save().then(referrer => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
referrerRoutes.route('/delete/:id').get(function (req, res) {
   Referrer.findOneAndDelete({_id: req.params.id}, function(err, referrer){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Referrer + ' using id ' + req.params.id );
    });
});

module.exports = referrerRoutes;