// responseOptionRoutes.js

var express = require('express');
var app = express();
var responseOptionRoutes = express.Router();

// Require Item model in our routes module
var ResponseOption = require('../models/ResponseOption');

// Defined store route
responseOptionRoutes.route('/add').post(function (req, res) {
	var responseOption = new ResponseOption(req.body);
	responseOption.save()
    .then(item => {
    	res.status(200).json({'responseOption': 'ResponseOption added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
responseOptionRoutes.route('/').get(function (req, res) {
	ResponseOption.find(function (err, responseOptions){
		if(err){
			console.log(err);
		}
		else {
			res.json(responseOptions);
		}
	});
});

// Defined edit route
responseOptionRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	ResponseOption.findById(id, function (err, responseOption){
		res.json(responseOption);
	});
});

//  Defined update route
responseOptionRoutes.route('/update/:id').post(function (req, res) {
	ResponseOption.findById(req.params.id, function(err, responseOption) {
		if (!responseOption)
			return next(new Error('Could not load a ResponseOption Document using id ' + req.params.id));
		else {
            responseOption.responseText = req.body.responseText;
            responseOption.gotoQuestionId = req.body.gotoQuestionId;

			responseOption.save().then(responseOption => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
responseOptionRoutes.route('/delete/:id').get(function (req, res) {
   ResponseOption.findOneAndDelete({_id: req.params.id}, function(err, responseOption){
        if(err) res.json(err);
        else res.json('Successfully removed ' + ResponseOption + ' using id ' + req.params.id );
    });
});

module.exports = responseOptionRoutes;