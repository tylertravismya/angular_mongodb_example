// referenceGiverRoutes.js

var express = require('express');
var app = express();
var referenceGiverRoutes = express.Router();

// Require Item model in our routes module
var ReferenceGiver = require('../models/ReferenceGiver');

// Defined store route
referenceGiverRoutes.route('/add').post(function (req, res) {
	var referenceGiver = new ReferenceGiver(req.body);
	referenceGiver.save()
    .then(item => {
    	res.status(200).json({'referenceGiver': 'ReferenceGiver added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
referenceGiverRoutes.route('/').get(function (req, res) {
	ReferenceGiver.find(function (err, referenceGivers){
		if(err){
			console.log(err);
		}
		else {
			res.json(referenceGivers);
		}
	});
});

// Defined edit route
referenceGiverRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	ReferenceGiver.findById(id, function (err, referenceGiver){
		res.json(referenceGiver);
	});
});

//  Defined update route
referenceGiverRoutes.route('/update/:id').post(function (req, res) {
	ReferenceGiver.findById(req.params.id, function(err, referenceGiver) {
		if (!referenceGiver)
			return next(new Error('Could not load a ReferenceGiver Document using id ' + req.params.id));
		else {
            referenceGiver.dateLastUpdated = req.body.dateLastUpdated;
            referenceGiver.active = req.body.active;
            referenceGiver.dateTimeLastViewedExternally = req.body.dateTimeLastViewedExternally;
            referenceGiver.makeViewableToUser = req.body.makeViewableToUser;
            referenceGiver.dateLastSent = req.body.dateLastSent;
            referenceGiver.numDaysToExpire = req.body.numDaysToExpire;
            referenceGiver.QuestionGroup = req.body.QuestionGroup;
            referenceGiver.Status = req.body.Status;
            referenceGiver.Type = req.body.Type;
            referenceGiver.User = req.body.User;
            referenceGiver.Referrer = req.body.Referrer;
            referenceGiver.Answers = req.body.Answers;
            referenceGiver.LastQuestionAnswered = req.body.LastQuestionAnswered;

			referenceGiver.save().then(referenceGiver => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
referenceGiverRoutes.route('/delete/:id').get(function (req, res) {
   ReferenceGiver.findOneAndDelete({_id: req.params.id}, function(err, referenceGiver){
        if(err) res.json(err);
        else res.json('Successfully removed ' + ReferenceGiver + ' using id ' + req.params.id );
    });
});

module.exports = referenceGiverRoutes;