// questionGroupRoutes.js

var express = require('express');
var app = express();
var questionGroupRoutes = express.Router();

// Require Item model in our routes module
var QuestionGroup = require('../models/QuestionGroup');

// Defined store route
questionGroupRoutes.route('/add').post(function (req, res) {
	var questionGroup = new QuestionGroup(req.body);
	questionGroup.save()
    .then(item => {
    	res.status(200).json({'questionGroup': 'QuestionGroup added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
questionGroupRoutes.route('/').get(function (req, res) {
	QuestionGroup.find(function (err, questionGroups){
		if(err){
			console.log(err);
		}
		else {
			res.json(questionGroups);
		}
	});
});

// Defined edit route
questionGroupRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	QuestionGroup.findById(id, function (err, questionGroup){
		res.json(questionGroup);
	});
});

//  Defined update route
questionGroupRoutes.route('/update/:id').post(function (req, res) {
	QuestionGroup.findById(req.params.id, function(err, questionGroup) {
		if (!questionGroup)
			return next(new Error('Could not load a QuestionGroup Document using id ' + req.params.id));
		else {
            questionGroup.aggregateScore = req.body.aggregateScore;
            questionGroup.weight = req.body.weight;
            questionGroup.name = req.body.name;
            questionGroup.Questions = req.body.Questions;
            questionGroup.QuestionGroups = req.body.QuestionGroups;

			questionGroup.save().then(questionGroup => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
questionGroupRoutes.route('/delete/:id').get(function (req, res) {
   QuestionGroup.findOneAndDelete({_id: req.params.id}, function(err, questionGroup){
        if(err) res.json(err);
        else res.json('Successfully removed ' + QuestionGroup + ' using id ' + req.params.id );
    });
});

module.exports = questionGroupRoutes;