import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {ResponseOption} from '../models/ResponseOption';
import { BaseService } from './base.service';

@Injectable()
export class ResponseOptionService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	responseOption : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a ResponseOption 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addResponseOption(responseText, gotoQuestionId) : Promise<any> {
    	const uri = this.ormUrl + '/ResponseOption/add';
    	const obj = {
      		responseText: responseText,
			gotoQuestionId: gotoQuestionId
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all ResponseOption 
	// returns the results untouched as JSON representation of an
	// array of ResponseOption models
	// delegates via URI to an ORM handler
	//********************************************************************
	getResponseOptions() {
    	const uri = this.ormUrl + '/ResponseOption';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a ResponseOption 
	// returns the results untouched as a JSON representation of a
	// ResponseOption model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editResponseOption(id) {
    	const uri = this.ormUrl + '/ResponseOption/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a ResponseOption 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateResponseOption(responseText, gotoQuestionId, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/ResponseOption/update/' + id;
    	const obj = {
      		responseText: responseText,
			gotoQuestionId: gotoQuestionId
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a ResponseOption 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteResponseOption(id)  : Promise<any> {
    	const uri = this.ormUrl + '/ResponseOption/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	

	//********************************************************************
	// saveHelper - internal helper to save a ResponseOption
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/ResponseOption/update/' + this.responseOption._id;		
		
    	return this
      			.http
      			.post(uri, this.responseOption)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a ResponseOption
	//********************************************************************	
	loadHelper( id ) {
		this.editResponseOption(id)
        		.subscribe(res => {
        			this.responseOption = res;
      			});
	}
}