import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {User} from '../models/User';
import {ReferrerService} from '../services/Referrer.service';
import {ReferrerGroupService} from '../services/ReferrerGroup.service';
import {ReferenceGroupLinkService} from '../services/ReferenceGroupLink.service';
import { BaseService } from './base.service';

@Injectable()
export class UserService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	user : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a User 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks) : Promise<any> {
    	const uri = this.ormUrl + '/User/add';
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
      		emailAddress: emailAddress,
      		active: active,
      		Comments: Comments != null && Comments.length > 0 ? Comments : null,
      		password: password,
      		resumeLinkUrl: resumeLinkUrl,
      		linkedInUrl: linkedInUrl,
      		ReferenceProviders: ReferenceProviders != null && ReferenceProviders.length > 0 ? ReferenceProviders : null,
      		RefererGroups: RefererGroups != null && RefererGroups.length > 0 ? RefererGroups : null,
      		ReferenceReceivers: ReferenceReceivers != null && ReferenceReceivers.length > 0 ? ReferenceReceivers : null,
			ReferenceGroupLinks: ReferenceGroupLinks != null && ReferenceGroupLinks.length > 0 ? ReferenceGroupLinks : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all User 
	// returns the results untouched as JSON representation of an
	// array of User models
	// delegates via URI to an ORM handler
	//********************************************************************
	getUsers() {
    	const uri = this.ormUrl + '/User';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a User 
	// returns the results untouched as a JSON representation of a
	// User model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editUser(id) {
    	const uri = this.ormUrl + '/User/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a User 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/User/update/' + id;
    	const obj = {
      		firstName: firstName,
      		lastName: lastName,
      		emailAddress: emailAddress,
      		active: active,
      		Comments: Comments != null && Comments.length > 0 ? Comments : null,
      		password: password,
      		resumeLinkUrl: resumeLinkUrl,
      		linkedInUrl: linkedInUrl,
      		ReferenceProviders: ReferenceProviders != null && ReferenceProviders.length > 0 ? ReferenceProviders : null,
      		RefererGroups: RefererGroups != null && RefererGroups.length > 0 ? RefererGroups : null,
      		ReferenceReceivers: ReferenceReceivers != null && ReferenceReceivers.length > 0 ? ReferenceReceivers : null,
			ReferenceGroupLinks: ReferenceGroupLinks != null && ReferenceGroupLinks.length > 0 ? ReferenceGroupLinks : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a User 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteUser(id)  : Promise<any> {
    	const uri = this.ormUrl + '/User/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more referenceProvidersIds as a ReferenceProviders 
	// to a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addReferenceProviders( userId, referenceProvidersIds ): Promise<any> {

		// get the User
		this.loadHelper( userId );
				
		// split on a comma with no spaces
		var idList = referenceProvidersIds.split(',')

		// iterate over array of referenceProviders ids
		idList.forEach(function (id) {
			// read the Referrer		
			var referrer = new ReferrerService(this.http).editReferrer(id);	
			// add the Referrer if not already assigned
			if ( this.user.referenceProviders.indexOf(referrer) == -1 )
				this.user.referenceProviders.push(referrer);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more referenceProvidersIds as a ReferenceProviders 
	// from a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeReferenceProviders( userId, referenceProvidersIds ): Promise<any> {
		
		// get the User
		this.loadHelper( userId );

				
		// split on a comma with no spaces
		var idList 					= referenceProvidersIds.split(',');
		var referenceProviders 	= this.user.referenceProviders;
		
		if ( referenceProviders != null && referenceProvidersIds != null ) {
		
			// iterate over array of referenceProviders ids
			referenceProviders.forEach(function (obj) {				
				if ( referenceProvidersIds.indexOf(obj._id) > -1 ) {
					 // remove the Referrer
					this.user.referenceProviders.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more refererGroupsIds as a RefererGroups 
	// to a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addRefererGroups( userId, refererGroupsIds ): Promise<any> {

		// get the User
		this.loadHelper( userId );
				
		// split on a comma with no spaces
		var idList = refererGroupsIds.split(',')

		// iterate over array of refererGroups ids
		idList.forEach(function (id) {
			// read the ReferrerGroup		
			var referrerGroup = new ReferrerGroupService(this.http).editReferrerGroup(id);	
			// add the ReferrerGroup if not already assigned
			if ( this.user.refererGroups.indexOf(referrerGroup) == -1 )
				this.user.refererGroups.push(referrerGroup);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more refererGroupsIds as a RefererGroups 
	// from a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeRefererGroups( userId, refererGroupsIds ): Promise<any> {
		
		// get the User
		this.loadHelper( userId );

				
		// split on a comma with no spaces
		var idList 					= refererGroupsIds.split(',');
		var refererGroups 	= this.user.refererGroups;
		
		if ( refererGroups != null && refererGroupsIds != null ) {
		
			// iterate over array of refererGroups ids
			refererGroups.forEach(function (obj) {				
				if ( refererGroupsIds.indexOf(obj._id) > -1 ) {
					 // remove the ReferrerGroup
					this.user.refererGroups.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more referenceReceiversIds as a ReferenceReceivers 
	// to a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addReferenceReceivers( userId, referenceReceiversIds ): Promise<any> {

		// get the User
		this.loadHelper( userId );
				
		// split on a comma with no spaces
		var idList = referenceReceiversIds.split(',')

		// iterate over array of referenceReceivers ids
		idList.forEach(function (id) {
			// read the Referrer		
			var referrer = new ReferrerService(this.http).editReferrer(id);	
			// add the Referrer if not already assigned
			if ( this.user.referenceReceivers.indexOf(referrer) == -1 )
				this.user.referenceReceivers.push(referrer);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more referenceReceiversIds as a ReferenceReceivers 
	// from a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeReferenceReceivers( userId, referenceReceiversIds ): Promise<any> {
		
		// get the User
		this.loadHelper( userId );

				
		// split on a comma with no spaces
		var idList 					= referenceReceiversIds.split(',');
		var referenceReceivers 	= this.user.referenceReceivers;
		
		if ( referenceReceivers != null && referenceReceiversIds != null ) {
		
			// iterate over array of referenceReceivers ids
			referenceReceivers.forEach(function (obj) {				
				if ( referenceReceiversIds.indexOf(obj._id) > -1 ) {
					 // remove the Referrer
					this.user.referenceReceivers.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more referenceGroupLinksIds as a ReferenceGroupLinks 
	// to a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addReferenceGroupLinks( userId, referenceGroupLinksIds ): Promise<any> {

		// get the User
		this.loadHelper( userId );
				
		// split on a comma with no spaces
		var idList = referenceGroupLinksIds.split(',')

		// iterate over array of referenceGroupLinks ids
		idList.forEach(function (id) {
			// read the ReferenceGroupLink		
			var referenceGroupLink = new ReferenceGroupLinkService(this.http).editReferenceGroupLink(id);	
			// add the ReferenceGroupLink if not already assigned
			if ( this.user.referenceGroupLinks.indexOf(referenceGroupLink) == -1 )
				this.user.referenceGroupLinks.push(referenceGroupLink);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more referenceGroupLinksIds as a ReferenceGroupLinks 
	// from a User
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeReferenceGroupLinks( userId, referenceGroupLinksIds ): Promise<any> {
		
		// get the User
		this.loadHelper( userId );

				
		// split on a comma with no spaces
		var idList 					= referenceGroupLinksIds.split(',');
		var referenceGroupLinks 	= this.user.referenceGroupLinks;
		
		if ( referenceGroupLinks != null && referenceGroupLinksIds != null ) {
		
			// iterate over array of referenceGroupLinks ids
			referenceGroupLinks.forEach(function (obj) {				
				if ( referenceGroupLinksIds.indexOf(obj._id) > -1 ) {
					 // remove the ReferenceGroupLink
					this.user.referenceGroupLinks.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a User
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/User/update/' + this.user._id;		
		
    	return this
      			.http
      			.post(uri, this.user)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a User
	//********************************************************************	
	loadHelper( id ) {
		this.editUser(id)
        		.subscribe(res => {
        			this.user = res;
      			});
	}
}