import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ReferrerService } from './Referrer.service';

describe('ReferrerService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ReferrerService] });
	});

  it('should be created', () => {
    const service: ReferrerService = TestBed.get(ReferrerService);
    expect(service).toBeTruthy();
  });
});
