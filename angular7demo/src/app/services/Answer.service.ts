import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Answer} from '../models/Answer';
import {QuestionService} from '../services/Question.service';
import {ResponseOptionService} from '../services/ResponseOption.service';
import { BaseService } from './base.service';

@Injectable()
export class AnswerService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	answer : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Answer 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addAnswer(Question, Response) : Promise<any> {
    	const uri = this.ormUrl + '/Answer/add';
    	const obj = {
      		Question: Question != null && Question.length > 0 ? Question : null,
			Response: Response != null && Response.length > 0 ? Response : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Answer 
	// returns the results untouched as JSON representation of an
	// array of Answer models
	// delegates via URI to an ORM handler
	//********************************************************************
	getAnswers() {
    	const uri = this.ormUrl + '/Answer';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Answer 
	// returns the results untouched as a JSON representation of a
	// Answer model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editAnswer(id) {
    	const uri = this.ormUrl + '/Answer/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Answer 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateAnswer(Question, Response, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Answer/update/' + id;
    	const obj = {
      		Question: Question != null && Question.length > 0 ? Question : null,
			Response: Response != null && Response.length > 0 ? Response : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Answer 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteAnswer(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Answer/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a Question on a Answer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignQuestion( answerId, questionId ): Promise<any> {

		// get the Answer from storage
		this.loadHelper( answerId );
		
		// get the Question from storage
		var question 	= new QuestionService(this.http).editQuestion(questionId);
		
		// assign the Question		
		this.answer.question = question;
      		
		// save the Answer
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Question on a Answer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignQuestion( answerId ): Promise<any> {

		// get the Answer from storage
        this.loadHelper( answerId );
		
		// assign Question to null		
		this.answer.question = null;
      		
		// save the Answer
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a Response on a Answer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignResponse( answerId, responseId ): Promise<any> {

		// get the Answer from storage
		this.loadHelper( answerId );
		
		// get the ResponseOption from storage
		var responseOption 	= new ResponseOptionService(this.http).editResponseOption(responseId);
		
		// assign the Response		
		this.answer.response = responseOption;
      		
		// save the Answer
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Response on a Answer
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignResponse( answerId ): Promise<any> {

		// get the Answer from storage
        this.loadHelper( answerId );
		
		// assign Response to null		
		this.answer.response = null;
      		
		// save the Answer
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a Answer
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Answer/update/' + this.answer._id;		
		
    	return this
      			.http
      			.post(uri, this.answer)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Answer
	//********************************************************************	
	loadHelper( id ) {
		this.editAnswer(id)
        		.subscribe(res => {
        			this.answer = res;
      			});
	}
}