import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Question} from '../models/Question';
import {ResponseOptionService} from '../services/ResponseOption.service';
import { BaseService } from './base.service';

@Injectable()
export class QuestionService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	question : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Question 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses) : Promise<any> {
    	const uri = this.ormUrl + '/Question/add';
    	const obj = {
      		weight: weight,
      		questionText: questionText,
      		identifier: identifier,
      		mustBeAnswered: mustBeAnswered,
      		responseExclusive: responseExclusive,
			Responses: Responses != null && Responses.length > 0 ? Responses : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Question 
	// returns the results untouched as JSON representation of an
	// array of Question models
	// delegates via URI to an ORM handler
	//********************************************************************
	getQuestions() {
    	const uri = this.ormUrl + '/Question';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Question 
	// returns the results untouched as a JSON representation of a
	// Question model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editQuestion(id) {
    	const uri = this.ormUrl + '/Question/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Question 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Question/update/' + id;
    	const obj = {
      		weight: weight,
      		questionText: questionText,
      		identifier: identifier,
      		mustBeAnswered: mustBeAnswered,
      		responseExclusive: responseExclusive,
			Responses: Responses != null && Responses.length > 0 ? Responses : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Question 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteQuestion(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Question/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more responsesIds as a Responses 
	// to a Question
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addResponses( questionId, responsesIds ): Promise<any> {

		// get the Question
		this.loadHelper( questionId );
				
		// split on a comma with no spaces
		var idList = responsesIds.split(',')

		// iterate over array of responses ids
		idList.forEach(function (id) {
			// read the ResponseOption		
			var responseOption = new ResponseOptionService(this.http).editResponseOption(id);	
			// add the ResponseOption if not already assigned
			if ( this.question.responses.indexOf(responseOption) == -1 )
				this.question.responses.push(responseOption);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more responsesIds as a Responses 
	// from a Question
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeResponses( questionId, responsesIds ): Promise<any> {
		
		// get the Question
		this.loadHelper( questionId );

				
		// split on a comma with no spaces
		var idList 					= responsesIds.split(',');
		var responses 	= this.question.responses;
		
		if ( responses != null && responsesIds != null ) {
		
			// iterate over array of responses ids
			responses.forEach(function (obj) {				
				if ( responsesIds.indexOf(obj._id) > -1 ) {
					 // remove the ResponseOption
					this.question.responses.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a Question
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Question/update/' + this.question._id;		
		
    	return this
      			.http
      			.post(uri, this.question)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Question
	//********************************************************************	
	loadHelper( id ) {
		this.editQuestion(id)
        		.subscribe(res => {
        			this.question = res;
      			});
	}
}