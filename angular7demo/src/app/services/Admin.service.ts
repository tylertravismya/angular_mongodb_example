import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Admin} from '../models/Admin';
import {UserService} from '../services/User.service';
import {ReferenceEngineService} from '../services/ReferenceEngine.service';
import { BaseService } from './base.service';

@Injectable()
export class AdminService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	admin : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Admin 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addAdmin(loginId, password, Users, ReferenceEngines) : Promise<any> {
    	const uri = this.ormUrl + '/Admin/add';
    	const obj = {
      		loginId: loginId,
      		password: password,
      		Users: Users != null && Users.length > 0 ? Users : null,
			ReferenceEngines: ReferenceEngines != null && ReferenceEngines.length > 0 ? ReferenceEngines : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Admin 
	// returns the results untouched as JSON representation of an
	// array of Admin models
	// delegates via URI to an ORM handler
	//********************************************************************
	getAdmins() {
    	const uri = this.ormUrl + '/Admin';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Admin 
	// returns the results untouched as a JSON representation of a
	// Admin model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editAdmin(id) {
    	const uri = this.ormUrl + '/Admin/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Admin 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateAdmin(loginId, password, Users, ReferenceEngines, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Admin/update/' + id;
    	const obj = {
      		loginId: loginId,
      		password: password,
      		Users: Users != null && Users.length > 0 ? Users : null,
			ReferenceEngines: ReferenceEngines != null && ReferenceEngines.length > 0 ? ReferenceEngines : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Admin 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteAdmin(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Admin/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more usersIds as a Users 
	// to a Admin
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addUsers( adminId, usersIds ): Promise<any> {

		// get the Admin
		this.loadHelper( adminId );
				
		// split on a comma with no spaces
		var idList = usersIds.split(',')

		// iterate over array of users ids
		idList.forEach(function (id) {
			// read the User		
			var user = new UserService(this.http).editUser(id);	
			// add the User if not already assigned
			if ( this.admin.users.indexOf(user) == -1 )
				this.admin.users.push(user);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more usersIds as a Users 
	// from a Admin
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeUsers( adminId, usersIds ): Promise<any> {
		
		// get the Admin
		this.loadHelper( adminId );

				
		// split on a comma with no spaces
		var idList 					= usersIds.split(',');
		var users 	= this.admin.users;
		
		if ( users != null && usersIds != null ) {
		
			// iterate over array of users ids
			users.forEach(function (obj) {				
				if ( usersIds.indexOf(obj._id) > -1 ) {
					 // remove the User
					this.admin.users.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			
	//********************************************************************
	// adds one or more referenceEnginesIds as a ReferenceEngines 
	// to a Admin
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addReferenceEngines( adminId, referenceEnginesIds ): Promise<any> {

		// get the Admin
		this.loadHelper( adminId );
				
		// split on a comma with no spaces
		var idList = referenceEnginesIds.split(',')

		// iterate over array of referenceEngines ids
		idList.forEach(function (id) {
			// read the ReferenceEngine		
			var referenceEngine = new ReferenceEngineService(this.http).editReferenceEngine(id);	
			// add the ReferenceEngine if not already assigned
			if ( this.admin.referenceEngines.indexOf(referenceEngine) == -1 )
				this.admin.referenceEngines.push(referenceEngine);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more referenceEnginesIds as a ReferenceEngines 
	// from a Admin
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeReferenceEngines( adminId, referenceEnginesIds ): Promise<any> {
		
		// get the Admin
		this.loadHelper( adminId );

				
		// split on a comma with no spaces
		var idList 					= referenceEnginesIds.split(',');
		var referenceEngines 	= this.admin.referenceEngines;
		
		if ( referenceEngines != null && referenceEnginesIds != null ) {
		
			// iterate over array of referenceEngines ids
			referenceEngines.forEach(function (obj) {				
				if ( referenceEnginesIds.indexOf(obj._id) > -1 ) {
					 // remove the ReferenceEngine
					this.admin.referenceEngines.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a Admin
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Admin/update/' + this.admin._id;		
		
    	return this
      			.http
      			.post(uri, this.admin)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Admin
	//********************************************************************	
	loadHelper( id ) {
		this.editAdmin(id)
        		.subscribe(res => {
        			this.admin = res;
      			});
	}
}