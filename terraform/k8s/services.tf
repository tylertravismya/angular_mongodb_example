resource "kubernetes_service" "app-master" {
  metadata {
    name = "app-master"

    labels {
      app  = "angular7demodemo"
    }
  }

  spec {
    selector {
      app  = "angular7demo"
    }
    port {
      name        = "app-port"
      port        = 8080
      target_port = 8080
    }

    port {
      name        = "mysql-port"
      port        = 3606
      target_port = 3606
    }

    type = "LoadBalancer"
  }
  
}
